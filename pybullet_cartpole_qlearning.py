# author: Finn Jonas Peper
# created: 20210309
# inspired by: https://muetsch.io/cartpole-with-qlearning-first-experiences-with-openai-gym.html

import os
import gym
import time

import numpy as np
import matplotlib.pyplot as plt

from InvertedPendulum import InvertedPendulumEnv

from random import random, choice

env = InvertedPendulumEnv()


def discretize(val, lower, upper, n):
    # val (float): the value to be discretized
    # upper (float): the upper bound of expected values
    # lower (float): the lower bound of expected values
    # n (int): the number of discrete states
    assert upper > lower, "upper bound should be greater than lower"
    bandwidth = upper-lower
    val_discrete = int(np.floor((val-lower)/(bandwidth/(n-2))))
    if val < lower:
        return 0
    elif val > upper:
        return n-1
    else:
        return val_discrete+1


def epsilon_greedy_action_selection(state_action_values, epsilon):
    if random() > epsilon:
        return np.argmax(state_action_values)
    else:
        return choice([0, 1])


def epsilon_cooldown(i):
    epsilon_min = 0.1
    epsilon_max = 2
    decrease_length = 5000

    b = epsilon_max
    m = (epsilon_min-b)/decrease_length
    y = min(1, m*i+b)

    return max(epsilon_min, y)


def gamma_cooldown(i):

    gamma_min = 0.3
    gamma_max = 2
    decrease_length = 5000

    b = gamma_max
    m = (gamma_min-b)/decrease_length
    y = min(1, m*i+b)

    return max(gamma_min, y)

def save_heat_map(Q,episode): 

    path = r"imgs"

    Q_diff = np.abs(Q[:,:,0]-Q[:,:,1])

    policy = np.argmax(Q[:,:], axis= 2)

    fig, axs = plt.subplots(1,2, figsize=(20,5))

    fig.suptitle(f"Episode: {episode}")

    axs[0].set_title("Difference in q-values for both actions")
    axs[0].imshow(Q_diff)
    axs[0].set_xlabel("angle")
    axs[0].set_ylabel("angular velocity")

    axs[1].set_title("Policy $\pi$ (blue = left, red = right)")
    axs[1].imshow(policy, cmap = "bwr")
    axs[1].set_xlabel("anglular velocity")
    axs[1].set_ylabel("angle ")

    plt.savefig(f"{path}\\img_{episode}.png")

n_states_theta = 12
n_states_theta_dt = 24

Q = np.random.random((n_states_theta, n_states_theta_dt, 2))

observation = [0, 0, 0, 0]

show_every_n_episodes = np.arange(1,25000,1000)
#show_every_n_episodes = np.array([800,1000,1500,2000])

max_steps = 0
steps_list = []

discretize_bound_angle = .35
discretize_bound_angular_velocity = 8

for i in range(8001):
    observation = env.reset()

    obs_theta = discretize(observation[2], -discretize_bound_angle, discretize_bound_angle, n_states_theta)
    obs_theta_dt = discretize(observation[3], -discretize_bound_angular_velocity , discretize_bound_angular_velocity , n_states_theta_dt)

    learning_rate = gamma_cooldown(i)
    epsilon = epsilon_cooldown(i)
    
    discount = 1
    
    if i in show_every_n_episodes: 
        show=True
    else: 
        show=False

    if show: 
        print(f"learning rate: {learning_rate}")
        print(f"epsilon: {epsilon}")

    for t in range(200):  

        action = epsilon_greedy_action_selection(
            Q[obs_theta][obs_theta_dt], epsilon)

        observation, reward, done, info = env.step(action, visualize=show)

        obs_theta_new = discretize(observation[2], -discretize_bound_angle, discretize_bound_angle, n_states_theta)
        obs_theta_dt_new = discretize(observation[3], -discretize_bound_angular_velocity , discretize_bound_angular_velocity , n_states_theta_dt)

        # Q-learning
        if not done: 
            Q[obs_theta][obs_theta_dt][action] += learning_rate * (reward + discount* np.max( Q[obs_theta_new][obs_theta_dt_new]) - Q[obs_theta][obs_theta_dt][action])

        obs_theta = obs_theta_new
        obs_theta_dt = obs_theta_dt_new

        if (done) and not show: 
            steps_made = t
            break

    if show:
        print(f"episode: {i}")
        print(f"steps made: {steps_made}") 
        save_heat_map(Q, i)


    steps_list.append(steps_made)
    #print(f"LEADER: {max_steps}")
    #print(f"Episode finished after {t+1} timesteps")
    

_, ax = plt.subplots(1,1)

ax.plot(steps_list)
ax.set_xlabel("episode")
ax.set_ylabel("steps made")
plt.savefig("imgs/learning_progress.png")
