import os
import time

import pybullet as p
import numpy as np
import numpy as np
import matplotlib.pyplot as plt

from random import random, choice

class InvertedPendulumEnv:
  def __init__(self):
    
    self.magntitude= 15

    p.connect(p.GUI)  # or p.DIRECT for non-graphical version
    p.setGravity(0,0, -9.81)
    
    self.assembly_id = p.loadURDF("pendulum.urdf")

    p.setJointMotorControl2(bodyUniqueId= self.assembly_id, jointIndex=1, controlMode=p.POSITION_CONTROL,  force = 0 )
    p.setJointMotorControl2(bodyUniqueId= self.assembly_id, jointIndex=2, controlMode=p.POSITION_CONTROL,  force = 0 )


  def reset(self): 

    random_init = np.random.normal(0, .05, 1)

    p.resetJointState(self.assembly_id, 1, 0, 0) # prismatic
    p.resetJointState(self.assembly_id, 2, random_init, 0) # revolute

    random_error = np.random.normal(0, .05, 4)

    rail_pos = p.getJointState(self.assembly_id, 1)[0] + random_error[0]
    rail_vel = p.getJointState(self.assembly_id, 1)[1] + random_error[1]
    angle_pos = p.getJointState(self.assembly_id, 2)[0] + random_error[2]
    angle_vel = p.getJointState(self.assembly_id, 2)[1] + random_error[3]

    observation = [rail_pos, rail_vel, angle_pos, angle_vel]

    return observation

  def step(self, action, visualize = False):
    if action == 0: 
      p.applyExternalForce(self.assembly_id, 1, forceObj=[+self.magntitude,0,0], posObj=[0,0,0], flags= p.WORLD_FRAME)
    else: # action == 1 
      p.applyExternalForce(self.assembly_id, 1, forceObj=[-self.magntitude,0,0], posObj=[0,0,0], flags= p.WORLD_FRAME)

    p.stepSimulation()

    random_error = np.random.normal(0, .05, 4)

    rail_pos = p.getJointState(self.assembly_id, 1)[0] + random_error[0]
    rail_vel = p.getJointState(self.assembly_id, 1)[1] + random_error[1]
    angle_pos = p.getJointState(self.assembly_id, 2)[0] + random_error[2]
    angle_vel = p.getJointState(self.assembly_id, 2)[1] + random_error[3]

    observation = [rail_pos, rail_vel, angle_pos, angle_vel]

    criteria_angle = abs(angle_pos) > np.deg2rad(15)
    criteria_position = False # abs(rail_pos) > 5

    done = criteria_angle or criteria_position

    if not done: 
      reward = 1
    else: 
      reward = 0      

    info = None 

    if visualize:
      time.sleep(1/240.)

    return observation, reward, done, info

  def disconnect(self):
    p.disconnect() 

'''
env = InvertedPendulumEnv()
env.reset()

for i in range(1000): 
  env.step(1)
  time.sleep(1/240.)

'''